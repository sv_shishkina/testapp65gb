package ru.sv_shishkina.testapp65gb.ui.screen.specialties;


import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.redmadrobot.chronos.ChronosOperation;
import com.redmadrobot.chronos.ChronosOperationResult;

import java.util.Collections;
import java.util.List;

import ru.sv_shishkina.testapp65gb.domain.OperationResponse;
import ru.sv_shishkina.testapp65gb.domain.datasource.network.models.Specialty;
import ru.sv_shishkina.testapp65gb.domain.repositories.EmployeesRepository;

public class GetSpecialtiesOperation extends ChronosOperation<OperationResponse<List<Specialty>>> {
    final private EmployeesRepository employeesRepository;

    public GetSpecialtiesOperation(EmployeesRepository employeesRepository) {
        this.employeesRepository = employeesRepository;
    }

    @Nullable
    @Override
    public OperationResponse<List<Specialty>> run() {
        final OperationResponse<List<Specialty>> response = new OperationResponse<>();
        try {
            response.setSuccessful(true);
            response.setResult(employeesRepository.getSpecialties());
            response.setError(null);
        } catch (Throwable e) {
            response.setSuccessful(false);
            response.setResult(Collections.<Specialty>emptyList());
            response.setError(e);
        }

        return response;
    }

    @NonNull
    @Override
    public Class<? extends ChronosOperationResult<OperationResponse<List<Specialty>>>> getResultClass() {
        return Result.class;
    }

    public final static class Result extends ChronosOperationResult<OperationResponse<List<Specialty>>> {
    }
}
