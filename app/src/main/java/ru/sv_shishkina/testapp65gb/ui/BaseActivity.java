package ru.sv_shishkina.testapp65gb.ui;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import ru.sv_shishkina.testapp65gb.App;
import ru.sv_shishkina.testapp65gb.R;
import ru.sv_shishkina.testapp65gb.di.components.ActivityComponent;
import ru.sv_shishkina.testapp65gb.di.components.ApplicationComponent;
import ru.sv_shishkina.testapp65gb.di.modules.ActivityModule;
import ru.sv_shishkina.testapp65gb.utils.navigator.NavigationHandler;

public abstract class BaseActivity extends AppCompatActivity {
    private ActivityComponent activityComponent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        activityComponent =
                this.getAppComponent().plus(new ActivityModule(new NavigationHandlerImpl()));
        super.onCreate(savedInstanceState);
        setupComponent(getAppComponent());
    }

    protected ApplicationComponent getAppComponent() {
        return App.from(this).getApplicationComponent();
    }

    public abstract void setupComponent(ApplicationComponent applicationComponent);

    public ActivityComponent getActivityComponent() {
        return activityComponent;
    }

    private final class NavigationHandlerImpl implements NavigationHandler {
        @Override
        public void replaceWithBackStack(Fragment fragment, String backStack) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fl_content, fragment)
                    .addToBackStack(backStack)
                    .commit();
        }
    }
}
