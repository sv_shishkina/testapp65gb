package ru.sv_shishkina.testapp65gb.ui.screen.employeedetails;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import ru.sv_shishkina.testapp65gb.R;
import ru.sv_shishkina.testapp65gb.di.components.ActivityComponent;
import ru.sv_shishkina.testapp65gb.di.modules.OperationsModule;
import ru.sv_shishkina.testapp65gb.domain.OperationResponse;
import ru.sv_shishkina.testapp65gb.domain.datasource.network.models.Employee;
import ru.sv_shishkina.testapp65gb.domain.datasource.network.models.Specialty;
import ru.sv_shishkina.testapp65gb.ui.BaseFragment;
import ru.sv_shishkina.testapp65gb.ui.activity.MainActivity;
import ru.sv_shishkina.testapp65gb.utils.Tools;

@FragmentWithArgs
public class EmployeeDetailsFragment extends BaseFragment {
    @BindString(R.string.name_template)
    String nameTemplate;
    @BindString(R.string.birthday_template)
    String birthdayTemplate;

    @BindView(R.id.sdv_photo)
    SimpleDraweeView sdvPhoto;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_age)
    TextView tvAge;
    @BindView(R.id.tv_specialty1)
    TextView tvSpecialty1;
    @BindView(R.id.tv_specialty2)
    TextView tvSpecialty2;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Inject
    GetEmployeeOperation getEmployeeOperation;

    @Arg
    Long employeeId;

    private Employee employee;

    public EmployeeDetailsFragment() {
        // Required empty public constructor
    }

    public static EmployeeDetailsFragment newInstance(Long employeeId) {
        return EmployeeDetailsFragmentBuilder.newEmployeeDetailsFragment(employeeId);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_employee_details, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getEmployeeOperation.setEmployeeId(employeeId);
        runOperation(getEmployeeOperation);

        toolbar.setNavigationIcon(R.mipmap.arrow);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStackImmediate();
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        hideActivityToolbar();
    }

    @Override
    public void onStop() {
        showActivityToolbar();
        super.onStop();
    }

    private void hideActivityToolbar() {
        Toolbar toolbar = ((MainActivity) getActivity()).getToolbar();
        toolbar.setVisibility(View.GONE);
    }

    private void showActivityToolbar() {
        Toolbar toolbar = ((MainActivity) getActivity()).getToolbar();
        toolbar.setVisibility(View.VISIBLE);
    }

    @Override
    public void setupComponent(ActivityComponent activityComponent) {
        getActivityComponent().plus(new OperationsModule()).inject(this);
    }

    private void showEmployeeInfo() {
        sdvPhoto.setImageURI(employee.getPhoto());
        tvName.setText(Tools.normalizeName(nameTemplate, employee.getFirstName(), employee.getLastName()));

        if (employee.getBirthday() != null && !employee.getBirthday().isEmpty()) {
            setAge();
        }

        setSpecialties();
    }

    private void setAge() {
        String age;
        String birthday = employee.getBirthday().replace("-", ".");

        if (employee.getBirthday() != null && !employee.getBirthday().isEmpty()) {
            age = String.format(birthdayTemplate, birthday, Tools.getAge(employee.getBirthday()));
        } else {
            age = String.format(birthdayTemplate, birthday, "-");
        }
        tvAge.setText(age);
    }

    private void setSpecialties() {
        List<Specialty> specialties = employee.getSpecialties();

        tvSpecialty1.setText(specialties.get(0).getTitle());

        if (specialties.size() == 2) {
            tvSpecialty2.setText(specialties.get(1).getTitle());
        }
    }

    public void onOperationFinished(final GetEmployeeOperation.Result result) {
        if (!result.isSuccessful()) {
            return;
        }

        final OperationResponse<Employee> response = result.getOutput();

        if (!response.getSuccessful()) {
            return;
        }

        final Employee employee = response.getResult();

        if (employee == null) {
            return;
        }

        this.employee = employee;
        showEmployeeInfo();
    }
}
