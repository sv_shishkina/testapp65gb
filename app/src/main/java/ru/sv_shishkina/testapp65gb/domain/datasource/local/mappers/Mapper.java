package ru.sv_shishkina.testapp65gb.domain.datasource.local.mappers;


public interface Mapper<Model, Data> {
    Model dataToModel(Data data);

    Data modelToData(Model model);
}
