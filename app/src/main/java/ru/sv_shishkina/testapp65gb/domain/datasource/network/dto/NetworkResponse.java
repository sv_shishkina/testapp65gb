package ru.sv_shishkina.testapp65gb.domain.datasource.network.dto;


import com.google.gson.annotations.SerializedName;

import java.util.List;

import ru.sv_shishkina.testapp65gb.domain.datasource.network.models.Employee;


public class NetworkResponse {
    @SerializedName("response")
    private List<Employee> employees;

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }
}
