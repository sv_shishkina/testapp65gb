package ru.sv_shishkina.testapp65gb.domain.datasource.local.models;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

@Entity
public class DBSpecialty {
    @Id
    private Long id;
    private String title;
    @Generated(hash = 1903049730)
    public DBSpecialty(Long id, String title) {
        this.id = id;
        this.title = title;
    }
    @Generated(hash = 829821464)
    public DBSpecialty() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getTitle() {
        return this.title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
}
