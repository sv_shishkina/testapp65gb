package ru.sv_shishkina.testapp65gb.domain.datasource.network;


import java.util.List;

import ru.sv_shishkina.testapp65gb.domain.exceptions.NetworkException;
import ru.sv_shishkina.testapp65gb.domain.datasource.network.models.Employee;

public interface NetworkDataSource {
    List<Employee> getEmployeesFromNetwork() throws NetworkException;
}
