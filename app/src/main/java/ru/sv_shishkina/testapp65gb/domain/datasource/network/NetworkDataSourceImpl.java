package ru.sv_shishkina.testapp65gb.domain.datasource.network;


import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;
import ru.sv_shishkina.testapp65gb.domain.datasource.network.dto.NetworkResponse;
import ru.sv_shishkina.testapp65gb.domain.exceptions.NetworkException;
import ru.sv_shishkina.testapp65gb.domain.datasource.network.models.Employee;

public class NetworkDataSourceImpl implements NetworkDataSource {
    final private Service65Gb service65Gb;

    public NetworkDataSourceImpl(Service65Gb service65Gb) {
        this.service65Gb = service65Gb;
    }

    @Override
    public List<Employee> getEmployeesFromNetwork() throws NetworkException {
        final List<Employee> employees;

        try {
            final Response<NetworkResponse> response = service65Gb.getEmployees().execute();

            if (!response.isSuccessful()) {
                throw new NetworkException(response.message());
            }

            employees = response.body().getEmployees();

        } catch (IOException e) {
            throw new NetworkException(e.getMessage(), e.getCause());
        }

        return employees;
    }
}
