package ru.sv_shishkina.testapp65gb.di.components;

import dagger.Subcomponent;
import ru.sv_shishkina.testapp65gb.di.modules.ActivityModule;
import ru.sv_shishkina.testapp65gb.di.modules.OperationsModule;
import ru.sv_shishkina.testapp65gb.di.scopes.ActivityScope;
import ru.sv_shishkina.testapp65gb.ui.BaseActivity;
import ru.sv_shishkina.testapp65gb.ui.activity.MainActivity;
import ru.sv_shishkina.testapp65gb.utils.navigator.NavigationHandler;

@ActivityScope
@Subcomponent(modules = {
        ActivityModule.class
})
public interface ActivityComponent {
    void inject(BaseActivity baseActivity);
    void inject(MainActivity activity);

    NavigationHandler navigationHandler();

    ViewComponent plus(OperationsModule operationsModule);
}
