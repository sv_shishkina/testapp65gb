package ru.sv_shishkina.testapp65gb.di.modules;

import android.content.Context;


import java.io.File;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.sv_shishkina.testapp65gb.BuildConfig;
import ru.sv_shishkina.testapp65gb.domain.datasource.network.Service65Gb;

@Module
public class NetworkModule {
    private static final long TIMEOUT = 5;
    private static final TimeUnit TIME_UNIT_TIMEOUT = TimeUnit.SECONDS;

    @Singleton
    @Provides
    public OkHttpClient provideOkHttp(Context context) {
        final OkHttpClient.Builder okHttpClientBuilder =  new OkHttpClient.Builder()
                .writeTimeout(TIMEOUT, TIME_UNIT_TIMEOUT)
                .readTimeout(TIMEOUT, TIME_UNIT_TIMEOUT)
                .connectTimeout(TIMEOUT, TIME_UNIT_TIMEOUT)
                .cache(new Cache(new File(context.getCacheDir(), "ok_http"), 1024 * 1024 * 5));
        if(BuildConfig.DEBUG){
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            okHttpClientBuilder.addInterceptor(interceptor);
        }
        return okHttpClientBuilder.build();
    }

    @Provides
    @Singleton
    public Service65Gb provideService65Gb(OkHttpClient client) {
        return new Retrofit.Builder()
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(Service65Gb.ENDPOINT)
                .build()
                .create(Service65Gb.class);
    }
}
