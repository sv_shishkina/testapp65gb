package ru.sv_shishkina.testapp65gb.di.modules;


import dagger.Module;
import dagger.Provides;
import ru.sv_shishkina.testapp65gb.domain.datasource.local.mappers.EmployeeDbMapper;
import ru.sv_shishkina.testapp65gb.domain.datasource.local.mappers.Mapper;
import ru.sv_shishkina.testapp65gb.domain.datasource.local.mappers.SpecialtyDBMapper;
import ru.sv_shishkina.testapp65gb.domain.datasource.local.models.DBEmployee;
import ru.sv_shishkina.testapp65gb.domain.datasource.local.models.DBSpecialty;
import ru.sv_shishkina.testapp65gb.domain.datasource.network.models.Employee;
import ru.sv_shishkina.testapp65gb.domain.datasource.network.models.Specialty;

@Module
public class DBMappersModule {
    @Provides
    Mapper<Specialty, DBSpecialty> provideSpecialtyDBMapper() {
        return new SpecialtyDBMapper();
    }

    @Provides
    Mapper<Employee, DBEmployee> provideEmployeeDBMapper(Mapper<Specialty, DBSpecialty> dbSpecialtyMapper) {
        return new EmployeeDbMapper(dbSpecialtyMapper);
    }
}
