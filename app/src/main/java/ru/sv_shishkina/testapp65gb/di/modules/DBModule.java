package ru.sv_shishkina.testapp65gb.di.modules;

import android.app.Application;
import android.content.Context;

import org.greenrobot.greendao.database.Database;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.sv_shishkina.testapp65gb.domain.Constants;
import ru.sv_shishkina.testapp65gb.domain.datasource.local.models.DaoMaster;
import ru.sv_shishkina.testapp65gb.domain.datasource.local.models.DaoSession;

@Singleton
@Module
public class DBModule {

    @Provides
    @Singleton
    DaoMaster.DevOpenHelper provideDevOpenHelper(Application application) {
        return new DaoMaster.DevOpenHelper(application, Constants.DB_NAME);
    }

    @Provides
    @Singleton
    DaoSession provideDaoSession(DaoMaster.DevOpenHelper devOpenHelper) {
        Database db = devOpenHelper.getWritableDb();
        return new DaoMaster(db).newSession();
    }
}
